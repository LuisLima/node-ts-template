# Node typescript template image for docker
FROM node:18
LABEL mantainer="Luis Lima"

# Copy the package.json to install dependencies to avoid rebuilding this part if the package.json file hasn't changed
COPY package.json package-lock.json ./

# Install the dependencies and make the folder
RUN  npm install && mkdir /node-ts-template && mv ./node_modules ./node-ts-template

# Define the directory where the application files will be copied
WORKDIR /node-ts-template

# Define entry argument to build image that will be use to define node port when run instance
ARG PORT_BUILD=3000

# Define node env mode: development or production 
RUN echo NODE_ENV=development >> .env

# Define server port to be used by the container
RUN echo SERVER_PORT=$PORT_BUILD >> .env

# Define mongodb user
RUN echo MONGODB_USER=admin >> .env

# Define mongodb password
RUN echo MONGODB_PASS=admin >> .env

# Define mongodb connection string
RUN echo MONGODB_CONN="mongodb://{{username}}:{{password}}@mongodb:27017/admin" >> .env

# Define Log Level 
RUN echo LOG_LEVEL=DEBUG >> .env

# Define log notification
RUN echo LOG_IS_TO_BE_NOTIFIED=false >> .env

# Define timeout to execute tasks em seconds
RUN echo TASK_TIMEOUT=1  >> .env

# Print .env file
RUN cat .env

# Expose the output port for application access
EXPOSE $PORT_BUILD

# Copy the project content to the folder defined by "WORKDIR" (see above)
COPY . .

# Execute the application
ENTRYPOINT npm start
