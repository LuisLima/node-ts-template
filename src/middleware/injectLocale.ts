
import * as express from "express";
import Context from "../route/Context";
import Logger from "../util/logger";

export const register = (app: express.Application) => {
    const TAG = 'injectLocale';       
    Logger.debug(TAG, `.${register.name} starting`);
    app.use((req: express.Request, res: any, next: any)=>{
        try {
            Logger.debug(TAG, `. executing`);
            const  PT_BR = 'pt-BR';
            let acceptLanguage = req.headers["accept-language"];
            if (!acceptLanguage) acceptLanguage = PT_BR;
            else {
                const EN = 'en', EN_US1 = "en-US", EN_US2 = "en_US"
                acceptLanguage = (acceptLanguage.includes(EN) || 
                                 acceptLanguage.includes(EN_US1) || 
                                 acceptLanguage.includes(EN_US2)) ? EN_US1 : PT_BR;				
            }
            const context = Context.get(req);
            if (context){
                context.dataMap['locale'] = acceptLanguage;                
                Logger.debug(TAG, `set locale = ${acceptLanguage}`);

            }else{                
                Logger.info(TAG,`no context defined`);
            }
        }catch(e){
            const unknownErr = e as Error;
            Logger.error(TAG, `${unknownErr.message}\nstack: ${unknownErr.stack}`);
        }finally{
            next();
        }                
    });
}
