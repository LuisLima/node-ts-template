import * as express from "express";
import Bundle from "../i18n/bundle";
import Logger from "../util/logger";
export const register = (app: express.Application) => {
    const TAG = 'requestInjector';    
    Logger.debug(TAG, `.${register.name} starting`);    
    app.use((req: express.Request, res: any, next: any )=> {
        
        Logger.debug(TAG, ` ${Bundle.TAG}.${Bundle.setRequest.name}`);
        Bundle.setRequest(req);
        next();
    });
}