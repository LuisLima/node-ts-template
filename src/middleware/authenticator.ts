import * as express from 'express';
import unless from 'express-unless';
import {StatusCodes} from 'http-status-codes';

import AppConfig from '../appConfig';
import Logger from '../util/logger';
import TokenModel, { Token } from '../model/token' 
import AuthenticationException from '../exception/AuthenticationException';
import Bundle from '../i18n/bundle';
import Context from '../route/Context';
import Url from '../route/url';
import TokenNoDefinedException from '../exception/tokenNoDefinedException';
import TokenNotFoundException from '../exception/tokenNotFoundException';
import InvalidTokenException from '../exception/invalidTokenException';
export const register = (app: express.Application) => {
    const auth = new Authenticator(); 
    app.use(auth.addCredential);
    app.use(auth.isAuthenticated);
}

class Authenticator {
    TAG = Authenticator.name;
    private securityRouteRegex: RegExp = new RegExp(`${Url.ROUTE_SECURITY_LOGON}.*` , 'g');
    private apiSwaggerDocRegex: RegExp = new RegExp(`${Url.API_DOC}.*` , 'g');
    openPath: unless.pathFilter[] = [
        this.securityRouteRegex,
        this.apiSwaggerDocRegex,
        Url.BASE_ROUTE_API,
        "/",
        "/favicon.ico",
        "/image\*"
    ];    
    constructor(){
        this.addCredential.unless = unless;
        this.isAuthenticated.unless = unless;

        this.addCredential = this.addCredential.unless({path: this.openPath});
        this.isAuthenticated = this.isAuthenticated.unless({path: this.openPath});
    }

    private async verifyToken(req: express.Request){
        let token = "";
        let isTokenFound = true;
        const LOCAL_TAG = `${this.TAG} - ${req.url}`;
        try {
            Logger.debug(LOCAL_TAG, ` ${this.verifyToken.name} from header ${AppConfig.TOKEN.AUTH_HEADER}`);
            token = req.headers[AppConfig.TOKEN.AUTH_HEADER] as string;
            if (!token) throw new TokenNoDefinedException();
            const tokenModel = new TokenModel();
            
            let tokenError!:Error;
            const tokenData: Token | void = await tokenModel.validateToken(token).catch((err: Error) => {
                Logger.error(LOCAL_TAG, `.${this.verifyToken.name} ${err}`);
                tokenError = err;
            });
            if (!tokenData) {
                if (tokenError) throw new InvalidTokenException(tokenError.message, tokenError.stack);
                else throw new InvalidTokenException();
            }

            const tokenFound = await TokenModel.findOne({key: token}).catch((err: Error) => Logger.error(LOCAL_TAG, `.${this.verifyToken.name} ${err}`));            
            if (!tokenFound) {
                isTokenFound = false;
                throw new TokenNotFoundException();
            }

            return token;
        } catch (e) { 
            let err = e as Error;
            switch (err.name) {
                case "TokenExpiredError":
                    err = new AuthenticationException(Bundle.key('error.logon.session.expired').get()); break;
                case "JsonWebTokenError":
                    err = new AuthenticationException(Bundle.key('error.logon.session.invalid.token').get()); break;
            }
            try{
                if (token && isTokenFound){
                    Logger.debug(LOCAL_TAG, `.${this.verifyToken.name} try to delete token...`);
                    const deleteResult = await TokenModel.deleteOne({key: token});
                    Logger.debug(LOCAL_TAG, `.${this.verifyToken.name} delete result: ${JSON.stringify(deleteResult, AppConfig.JSON.NO_REPLACER, AppConfig.JSON.TWO_SPACES)}`);
                } 
            }catch(err){
                Logger.error(LOCAL_TAG, `.${this.verifyToken.name} error when trying to delete token: ${JSON.stringify(err, AppConfig.JSON.NO_REPLACER, AppConfig.JSON.TWO_SPACES)}`);    
            }
            Logger.error(LOCAL_TAG, `.${this.verifyToken.name} ${err}`);
            throw err;        
        }
    }

    addCredential = <unless.RequestHandler>(async (req: express.Request, res: express.Response, next: Function ) => {
        try{
            const token = await this.verifyToken(req);
            const context = Context.get(req);
            if (context) {
                context.dataMap['token'] = token;
            }
            next();
        }catch(err){
            res.status(StatusCodes.UNAUTHORIZED).send(err);
        }
    });


    isAuthenticated = <unless.RequestHandler>((req: express.Request, res: express.Response, next: Function ) => {
        try{            
            const context = Context.get(req);
            if (context && context.dataMap['token']) {                
                next();
            }else{
                let errorMessage = Bundle.key('error.logon.user.unauthenticated').get()
                Logger.error(this.TAG + req.url, `.${this.isAuthenticated.name} ${errorMessage}`);
                res.status(StatusCodes.UNAUTHORIZED).send(new AuthenticationException(errorMessage));
            }
            
        }catch(err){
            res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(err);
        }
    });
}
