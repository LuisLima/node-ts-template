import * as express from "express"

import Logger from '../util/logger';
import * as initContext from './initContext';
import * as requestInjector from './requestInjector';
import * as injectLocale from './injectLocale'
import * as authetication from './authenticator'
export const register = (app: express.Application) => {
    const TAG = 'middleware';
    Logger.debug(TAG, `.${register.name} starting`);
    initContext.register(app);
    requestInjector.register(app);
    injectLocale.register(app);
    authetication.register(app);
};