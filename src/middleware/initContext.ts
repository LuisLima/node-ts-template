import * as express from "express";
import Context from "../route/Context";
import Logger from "../util/logger";
export const register = (app: express.Application) => {
    const TAG = 'initContext';    
    Logger.debug(TAG, `.${register.name} starting`);
    app.use((req: express.Request, res: any, next: any)=>{
        try{
            Logger.debug(TAG, `. ${req.url} `);
            Context.bind(req);
        }catch(e){
            const err = e as Error;
            Logger.error(TAG, err.message);
        }finally{
            next();
        }
    });
}