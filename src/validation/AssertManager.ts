import BusinessException from "../exception/businessException";
import Bundle from "../i18n/bundle";
import AppAssert from "./appAssert";

class AssertManager {        
    
    private _assetMap: {[k:string]:AppAssert} = {};

    add(asset: AppAssert){
        this._assetMap[asset.getMessage() as string] = asset;
        return this;
    };

    verifyAll(){
        var messages = Object.getOwnPropertyNames(this._assetMap)
            .filter(property => !this._assetMap[property].isValid())
            .map(property => this._assetMap[property].getMessage())
            .sort((message1:string, message2:string) => message1.localeCompare(message2));
        this._assetMap = {};
        if (messages && messages.length > 0){
            throw new BusinessException(messages.join('\n'));
        }
    };
};
export default new AssertManager();