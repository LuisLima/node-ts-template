import BusinessException from "../exception/businessException";
import Bundle from "../i18n/bundle";

function createInstance<A extends Error> (type: (new (...args: any[]) => A),  ...args: any[]): A {
    return new type(...args);
  }

function createInstanceError<A extends Error> (type: (new (message: string, stack: string | undefined) => A),  message: string, stack: string | undefined = undefined): A {
    return new type(message, stack);
  }

  export default  class AppAssert {
    private valid: boolean = false;
    private message: string = "";
    private messageReplacer: Array<string | number | boolean> = [];
    private exceptionClassName: string | undefined;
    

    constructor(expression: boolean){
        this.check(expression);
    }
    isValid(): boolean{
        return this.valid;
    }

    getMessage(): string {
        return  this.message;
    }

    check(expression: boolean): AppAssert {
        this.valid = expression;
        return this;
    }

    orFail(message: string, messageReplacer: string[] | undefined = undefined, exceptionClassName:string | undefined = undefined): AppAssert {
        if (messageReplacer && messageReplacer.length > 0){
            this.messageReplacer = this.messageReplacer.concat(messageReplacer);
        }

        if(exceptionClassName){
            this.exceptionClassName = exceptionClassName;
        }else{
            this.exceptionClassName = BusinessException.name;
        }
        this.message = message.trim();
        if (this.message.length == 0) throw new BusinessException('error.message.is.no.defined');
        return this;
    }

    verify(){
        if (!this.valid){
            if (this.message && this.message.length > 0){
                const messageResult = Bundle.key(this.message).replacerList(this.messageReplacer).get();                                
                throw new BusinessException(messageResult);                
            }else{
                throw new BusinessException(Bundle.key("error.message.is.no.defined").get());  
            }
        }
    }
}

