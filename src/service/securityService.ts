import AuthenticationDTO from '../dto/autenticationDTO';
import AuthenticationException from '../exception/AuthenticationException';
import ExceptionUtil from "../exception/exceptionUtil";
import Bundle from '../i18n/bundle';
import TokenModel, { TokenDocument } from "../model/token";
import Text from '../util/text';
import AppAssert from '../validation/appAssert';

class SecurityServiceType {    
    TAG: string = SecurityServiceType.name;

    authenticate(authDTO: AuthenticationDTO, jsessionId: string): Promise<string> {
        return new Promise(async (resolve, reject) => {

                try{
                    new AppAssert(authDTO != undefined).orFail("error.authentication.data.is.no.defined").verify();
                    new AppAssert(!Text.isEmpty(authDTO.username)).orFail("error.logon.username.is.no.defined").verify();
                    new AppAssert(!Text.isEmpty(authDTO.password)).orFail("error.logon.password.is.no.defined").verify();
                    new AppAssert(!Text.isEmpty(jsessionId)).orFail("error.jsession.is.no.defined").verify();
                    if (authDTO.username == "admin" && authDTO.password == "admin"){ //Change this to your own authentication logic
                        const token = TokenModel.generateToken(authDTO.username, jsessionId);        
                        const tokenModel = new TokenModel();
                        
                        await tokenModel.fillModel(token);
                        tokenModel.save().then((result: TokenDocument) => {
                            resolve(result.key);
                        })
                        .catch((error: Error) => {
                            ExceptionUtil.handleException(this.TAG, this.authenticate.name, error, reject);
                        });
                    }else{
                        reject(new AuthenticationException(Bundle.key("error.logon.username.or.password.invalid").get()));
                    }
                }catch(e){      
                    const err = e as Error;
                    ExceptionUtil.handleException(this.TAG, this.authenticate.name, err, reject);
                }
        });
    }    
}

const SecurityService = new SecurityServiceType();
export default SecurityService;