import { IncomingMessage, Server } from 'http';

import { Socket } from 'net';
import * as WebSocket from 'ws';
import AppConfig from '../appConfig';
import Notification from '../model/notification';
import Logger from '../util/logger';

const extractRadarToken = function extractToken(str:string){
    let regex = new RegExp(`${AppConfig.TOKEN.AUTH_HEADER}=.*`, 'gi');
    let m;
    let result: string[] = [];
    while ((m = regex.exec(str)) !== null) {
        // This is necessary to avoid infinite loops with zero-width matches
        if (m.index === regex.lastIndex) {
            regex.lastIndex++;
        }
        result  = m.map((match) => match);
    }
    if (result.length == 1) return result[0].split("=")[1];
    else return result;        
};

/**
 * A classe Logger usa o NotificationService; 
 * Ao utilizar a classe Logger em NotificationService, 
 * deve-se informar em todos os métodos de impressão o parâmetro 'notify' como 'false';
 * Caso contrário, há o RISCO DE LOOP INFINITO.
 */
const NO_NOTIFICATION = false;


class NotificationServiceType {
    TAG: string = NotificationServiceType.name;
    wsServer!: WebSocket.Server;

    start(server: Server){         
        Logger.info(this.TAG, `.${this.start.name} starting...`, NO_NOTIFICATION);
        if (!this.wsServer){
            this.wsServer = new WebSocket.Server({noServer: true});
            server.on('upgrade', async (request: IncomingMessage, socket: Socket, upgradeHead: Buffer) => {

                //HABILITAR o código abaixo quando for proteger o acesso ao websocket
                
                // let result = false;
                // try {
                //     if (request.url)   {                            
                //         const X_TOKEN_RADAR = extractRadarToken(request.url);
                //         if (X_TOKEN_RADAR) request.headers[AppConfig.AUTH_TOKEN_HEADER] = X_TOKEN_RADAR;
                //     }
                //     result = await Auth.isTokenValid(request);
                // }catch(err){
                //     Logger.error(this.TAG, `: server.upgrade error ${err.message} + "\nstack: ${err.stack}`);
                //     socket.destroy();
                //     return;                       
                // }
                // if (result){
                //     this.wsServer.handleUpgrade(request, socket, head, ws =>{
                //         this.wsServer.emit('connection', ws);
                //     });
                // }else{
                //     socket.destroy();
                //     Logger.info(this.TAG, `: server.upgrade token to websocket is invalid`);
                // }

                try{
                    // const lock = request.headers["sec-websocket-key"] || (request.socket.address() as AddressInfo).address;
                    // const hash = sha256(lock);                 
                    // const newKey = Base64.stringify(hmacSHA512(hash, AppConfig.TOKEN.KEY));
                    this.printUpgradeParams(request);
    
                    //Remover o código abaixo quando for proteger o acesso ao websocket                
                    this.wsServer.handleUpgrade(request, socket, upgradeHead, ws => {
                        this.wsServer.emit('connection', ws);
                    });                    

                }catch(e){
                    const err = e as Error;
                    Logger.error(this.TAG, `: server.upgrade error ${err.message} + "\nstack: ${err.stack}`);
                    socket.destroy();
                    return;          
                }
            });

            this.wsServer.on('connection', ws => {
                try {
                    ws.on('message', message => {
                        if (message) {
                            let messageAsJSON = message;
                            try { 
                                messageAsJSON = JSON.stringify(message, AppConfig.JSON.NO_REPLACER, AppConfig.JSON.TWO_SPACES); 
                            } catch(stringifyError) { 
                                const err = stringifyError as Error;
                                Logger.error(this.TAG, `: ${ws.onmessage.name} error ${err.message} + "\nstack: ${err.stack}`);
                            }
                            Logger.info(this.TAG, `.websocket.message: Received message => ${messageAsJSON}`, NO_NOTIFICATION);
                        }
                        try{
                            const notification: Notification = {
                                data: {
                                    "message": "Ok",                            
                                },
                                eventName:"messageReceived"
        
                            }
                            ws.send(JSON.stringify(notification));
                        }catch(e){
                            const err = e as Error;
                            Logger.error(this.TAG, `: ${ws.onmessage.name} ws.${ws.send.name} error ${err.message} + "\nstack: ${err.stack}`);
                        }
                    });
    
                    ws.on('pong', function(dataBuffer) {                            
                        try{                            
                            //Logger.info(this.TAG, `.websocket.pong: ${JSON.stringify(dataBuffer, AppConfig.JSON.NO_REPLACER, AppConfig.JSON.TWO_SPACES)}`, NO_NOTIFICATION);
                            // Logger.info(NotificationServiceType.name, `ws.${ws.pong.name}: ${this.url}`, NO_NOTIFICATION);
                        } catch(stringifyError) { 
                            const err = stringifyError as Error;
                            Logger.error(NotificationServiceType.name, `: ${ws.pong.name} error ${err.message} + "\nstack: ${err.stack}`);
                        }
                    });
    
                    ws.on('close', () => {    
                        ws.terminate();                    
                        Logger.info(this.TAG, `.ws.${ws.close.name}: disconnected`, NO_NOTIFICATION);
                    });
                }catch(e){
                    const err = e as Error;
                    Logger.error(this.TAG, `: wsServer.on('connection', () => {}) error ${err.message} + "\nstack: ${err.stack}`);
                }

            });            

            const interval = setInterval(() => {
                this.wsServer.clients.forEach((ws) => {
                  if (ws.readyState == WebSocket.OPEN || ws.readyState == WebSocket.CONNECTING) {
                    //   Logger.info(this.TAG, `: ws.${ws.ping.name} to ${ws.url}`);
                      ws.ping();
                  }else{
                    return ws.terminate();                  
                  }
                });
              }, 30000);
        }
    }

    notify(notification: Notification) {
        Logger.debug(this.TAG, `.${this.notify.name} event ${notification.eventName}`);
        
        
        if (this.wsServer){
            this.wsServer.clients.forEach((client) => {
                if (client.readyState === WebSocket.OPEN) {
                    try{

                        client.send(JSON.stringify(notification, AppConfig.JSON.NO_REPLACER, AppConfig.JSON.TWO_SPACES));
                    }catch(e){
                        const err = e as Error;
                        Logger.error(this.TAG, `: client.${client.send.name} error ${err.message} + "\nstack: ${err.stack}`);
                    }
                }
            });            
        }
    }

    private printUpgradeParams(request: IncomingMessage){
        try{
            const info = {
                "client-address": request.socket.remoteAddress,
                headers: {
                    "sec-websocket-key": request.headers['sec-websocket-key'],
                    "sec-websocket-protocol": request.headers['sec-websocket-protocol'],
                    "sec-websocket-version": request.headers['sec-websocket-version'],
                    "sec-websocket-accept": request.headers['sec-websocket-accept'],
                    ...request.headers
                },                
            }
            Logger.debug(this.TAG, `${this.printUpgradeParams.name} info: ${JSON.stringify(info, AppConfig.JSON.NO_REPLACER, AppConfig.JSON.TWO_SPACES)}`);
        }catch(e){
            const err = e as Error;
            Logger.error(this.TAG, `${this.printUpgradeParams.name} ${err.message}\n stack: ${err.stack}`, NO_NOTIFICATION);
        }
    }      
};

const NotificationService = new NotificationServiceType();
export default NotificationService;