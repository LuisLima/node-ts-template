import {Types} from 'mongoose'

import AppConfig from '../appConfig';
import place, { Place, PlaceDocument, PLACE_FIELD_MAP} from "../model/place";
import PlaceDTO from "../dto/placeDTO";
import PlacePaginateResultDTO from "../dto/placePaginateResultDTO";
import ExceptionUtil from "../exception/exceptionUtil";
import AppAssert from '../validation/appAssert';
import BusinessException from '../exception/businessException';
import Bundle from '../i18n/bundle';

class PlaceServiceType {    
    TAG: string = PlaceServiceType.name;    
    public findByFilter(
            filter: PlaceDTO, 
            page: number = AppConfig.PAGINATION.page, 
            limit: number = AppConfig.PAGINATION.limit): Promise<PlacePaginateResultDTO | undefined> {
                
        return new Promise(async (resolve, reject) => {
            try{
                const fieldMap = PLACE_FIELD_MAP;
                const sortedBy: {[k: string]:number} = {};
                sortedBy[fieldMap.NAME] = 1;
                
                const select: {[k: string]:number} = {};
                 select[fieldMap.NAME] = 1;
                 select[fieldMap.FLOOR] = 1;
                 select[fieldMap.NUMBER] = 1;
                 select[fieldMap.DIMENSION] = 1;                
                 const query = place.find().select(select).sort(sortedBy);                 
                if (filter){
                    if (filter.floor != undefined){
                        const floor: {[k: string]:number} = {};
                        floor[fieldMap.FLOOR] = filter.floor;
                        query.where(floor);
                    }
                }
                
                new AppAssert(!Number.isNaN(page) && page > 0).orFail('error.page.invalid.number', [page.toString()]).verify();
                new AppAssert(!Number.isNaN(limit) && limit > 0).orFail('error.limit.invalid.number', [limit.toString()]).verify();
   
                let result = await place.paginate(query, {page, perPage: limit , lean:true})
                                        .catch((error: Error) => ExceptionUtil.handleException(this.TAG, this.findById.name, error, reject));                
                let placeResult = undefined;
                if (result){
                    placeResult = place.transform(result);
                }
                resolve(placeResult);
            }catch(e){      
                const err = e as Error;
                ExceptionUtil.handleException(this.TAG, this.findByFilter.name, err, reject);
            }
        });
    }

    findById(placeId: string): Promise<PlaceDTO | undefined> {
        return new Promise(async (resolve, reject) => {
            let placeResult = undefined;
            try {
                const id = new Types.ObjectId(placeId);
                const result = await place.findOne({"_id": id})
                                          .catch((error: Error) => ExceptionUtil.handleException(this.TAG, this.findById.name, error, reject));
                if (result){                    
                    placeResult = place.exportData(result);
                }
                resolve(placeResult);
            }catch(e){      
                const err = e as Error;
                ExceptionUtil.handleException(this.TAG, this.findById.name, err, reject);
            }
        }); 
    }

    save(placeDTO: PlaceDTO): Promise<PlaceDTO> {
        return new Promise(async (resolve, reject) => {
            let PlaceModel:PlaceDocument | null;
            if (!placeDTO.id){
                PlaceModel = new place();
            }else{
                PlaceModel = await place.findOne({"_id":  new Types.ObjectId(placeDTO.id)})
            }

            if (PlaceModel){
                PlaceModel.fillModel(placeDTO);
                PlaceModel.save()
                .then((result: PlaceDocument) => {
                    resolve(place.exportData(result));
                })
                .catch((error: Error) => {
                    ExceptionUtil.handleException(this.TAG, this.save.name, error, reject);
                });
            }else{
                const error = new BusinessException(Bundle.key("error.place.id.is.not.found").get());
                ExceptionUtil.handleException(this.TAG, this.save.name, error, reject);
            }
        });
    }    
}

const PlaceService = new PlaceServiceType();
export default PlaceService;