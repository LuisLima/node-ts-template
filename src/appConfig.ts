import * as dotenv from 'dotenv';
import  {Algorithm} from 'jsonwebtoken';
dotenv.config();
class AppConfigType{
    ENV:string = process.env.NODE_ENV || 'production';
    PORT:number = process.env.SERVER_PORT ? parseInt(process.env.SERVER_PORT) : 3000;

    JSON = {
        NO_REPLACER: null,
        TWO_SPACES: 2,        
        FOUR_SPACES: 4
    }; 
    
    TASK ={
        timeout: process.env.TASK_TIMEOUT ? parseInt(process.env.TASK_TIMEOUT) : 1 //em segundos
    };

    LOG = {
        level: process.env.LOG_LEVEL || "INFO",
        isLogToBeNotified:  process.env.LOG_IS_TO_BE_NOTIFIED == "true"
    };
        
    MONGODB = {
        "connectionString": process.env.MONGODB_CONN ? process.env.MONGODB_CONN.replace(/\"/g, "") : "mongodb://{{username}}:{{password}}@localhost:27017/test",
        "username": process.env.MONGODB_USER || "test",
        "password": process.env.MONGODB_PASS || "test",
        "autoIndex": false,
        "autoCreate": false,
        "poolSize": 10
    };

    PAGINATION = {
        "limit": 20,
        "page": 1
    };

    TOKEN = {
        SECRET: "=!ApPl1c4Ti0n", //chnage this value for a random string
        KEY:  "489ba6de-ed3e-4bdf-a5bf-dcad77886043",
        ALGORITHM:  (process.env.ALGORITHM  || "HS256") as Algorithm,
        MAX_AGE: 300000,
        ISSUER:  "app:issuer",          
        AUTH_HEADER: "x-token-app"      
    }
}

const AppConfig = new AppConfigType();
export default AppConfig;