import { Position } from "../model/position";

export default interface TokenDTO {    
    id?: string;
    key?: string;
    subject?: string
    jsessionId?: string
    algorithm?: string
    issuedAt?: string
    jwtid?: string;
    expiryDate?: number;
    notValidBefore?: number;
}