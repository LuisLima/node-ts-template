import { IPaginateResult } from "mongoose";
import Converter from "../model/converter";

export default abstract class PaginateResultDTO<T> implements Converter<IPaginateResult<T>, PaginateResultDTO<T>>{
    public limit: number | undefined;
    public page?: number | null | undefined;
    public totalPages: number | undefined;
    public nextPage?: number | null;
    public prevPage?: number | null;
    public hasPrevPage: boolean | undefined;
    public hasNextPage: boolean | undefined;

    constructor(){}
    
    convert(paginateResult: IPaginateResult<T>) : PaginateResultDTO<T>{
        this.limit = paginateResult.pagination.perPage;
        this.page = paginateResult.pagination.page;
        this.totalPages = paginateResult.pagination.totalPages;
        this.hasNextPage = paginateResult.pagination.hasNextPage;
        this.nextPage = paginateResult.pagination.nextPage;
        this.hasPrevPage = paginateResult.pagination.hasPrevPage;
        this.prevPage = paginateResult.pagination.prevPage;
        return this;
    }
}