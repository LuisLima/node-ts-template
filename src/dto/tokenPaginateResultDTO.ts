import { IPaginateResult } from "mongoose";
import Converter from "../model/converter";
import tokenModel, { TokenDocument } from "../model/token";
import PaginateResultDTO from "./paginateResultDTO";
import TokenDTO from "./tokenDTO";

export default class TokenPaginateResultDTO extends PaginateResultDTO<TokenDocument> implements Converter<IPaginateResult<TokenDocument>, TokenPaginateResultDTO>{
    public tokens: TokenDTO[] | undefined;

    constructor(){
        super();
    }

    convert(paginateResult: IPaginateResult<TokenDocument>) : TokenPaginateResultDTO{
        this.tokens = paginateResult.data && paginateResult.data.length > 0 ? paginateResult.data.map(doc => tokenModel.exportData(doc)) : [];
        super.convert(paginateResult);
        return this;
    }
}