import { IPaginateResult } from "mongoose";
import Converter from "../model/converter";
import place, { PlaceDocument} from "../model/place";
import PaginateResultDTO from "./paginateResultDTO";
import PlaceDTO from "./placeDTO";

export default class PlacePaginateResultDTO extends PaginateResultDTO<PlaceDocument> implements Converter<IPaginateResult<PlaceDocument>, PlacePaginateResultDTO>{
    public places: PlaceDTO[] | undefined;

    constructor(){
        super();
    }

    convert(paginateResult: IPaginateResult<PlaceDocument>) : PlacePaginateResultDTO{
        this.places = paginateResult.data && paginateResult.data.length > 0 ? paginateResult.data.map(doc => place.exportData(doc)) : [];
        super.convert(paginateResult);
        return this;
    }
}