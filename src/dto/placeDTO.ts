import { Position } from "../model/position";

export default interface PlaceDTO {    
    id?: string;
    name?: string;
    floor?: number;
    number?: number;
    dimension?: Position;
}