export default class AuthenticationException implements Error {
    name: string = AuthenticationException.name;
    public message: string;
    public stack?: string | undefined;
    constructor(message: string, stack: string | undefined = undefined){
        this.message = message;
        
        if (!stack) this.stack = (new Error()).stack;
        else this.stack = stack;

        Object.setPrototypeOf(this, new.target.prototype);        
    }


    toString(){
        return JSON.stringify(this);
    }
}