import Bundle from "../i18n/bundle";

export default class TokenNoDefinedException implements Error {
    name: string = TokenNoDefinedException.name;
    public message: string;
    public stack?: string | undefined;    

    constructor(message: string = Bundle.key('error.token.no.defined').get(), stack: string | undefined = undefined){
        this.message = message;
        
        if (!stack) this.stack = (new Error()).stack;
        else this.stack = stack;

        Object.setPrototypeOf(this, new.target.prototype);        
    }


    toString(){
        return JSON.stringify(this);
    }
}