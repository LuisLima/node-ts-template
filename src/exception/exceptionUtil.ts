import dateFormat from "dateformat";
import Logger from "../util/logger";
import BusinessException from "./businessException";

export default class ExceptionUtil {
    static handleException(tag: string, functionName:string, err: Error, rejectFunction: Function): void {
        try{
            Logger.error(tag, `.${functionName} ${err.message}\nstack: ${err.stack}`);
         }catch(e){
            const logError = e as Error;
             Logger.error(tag, `.${functionName} ${logError.message}\nstack: ${logError.stack}`);
         }finally{
             try{
                 
                 if (err instanceof BusinessException) return rejectFunction(err);
                 return rejectFunction(new BusinessException(err.message, err.stack));
             }catch(e){
                const unknownErr = e as Error;
                 try{
                    Logger.error(tag, `.${functionName} ${unknownErr.message}\nstack: ${unknownErr.stack}`);
                 }catch(e){
                    const logError = e as Error;
                     console.error(`[ERROR] ${dateFormat(new Date(), "isoDateTime")} ${tag}.${functionName} ${logError.message}\nstack: ${logError.stack}`);
                 }
                 return rejectFunction(err);
             }
        }
    }   
}
