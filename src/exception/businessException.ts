export default class BusinessException implements Error {
    name: string = BusinessException.name;
    public message: string;
    public stack?: string | undefined;
    constructor(message: string, stack: string | undefined = undefined){
        this.message = message;
        
        if (!stack) this.stack = (new Error()).stack;
        else this.stack = stack;

        Object.setPrototypeOf(this, new.target.prototype);        
    }


    toString(){
        return JSON.stringify(this);
    }
}