import mongoose, {ConnectOptions} from 'mongoose';
import AppConfig from '../appConfig';
import Logger from '../util/logger';

const TAG = "Respository";
const urlConnection = AppConfig.MONGODB.connectionString
                                .replace("{{username}}", AppConfig.MONGODB.username)
                                .replace("{{password}}", AppConfig.MONGODB.password);
var connection: Promise<typeof mongoose>;
class RepositoryType {
    start(): Promise<typeof mongoose> {
        return new Promise(async (resolve, reject) => {
            //Init connection poll            
            Logger.info(TAG, `.${this.start.name} configuring connection pool...`);
            this.getConnection().then(
                (data:any) => {
                    try{                        
                        Logger.info(TAG, `.${this.getConnection.name} configured!\n`);
                        //Logger.info(TAG, `.${this.getConnection.name} configured\n{\n${Object.getOwnPropertyNames(data).map(property => `\t${property}: ${data[property]}\n`).join()}\n}\n`);

                    }finally{
                        resolve(data);                        
                    }
                },
                (err:Error) => {
                    try{
                        Logger.error(TAG, `.${this.getConnection.name}: ${err.message} "\nstack: ${err.stack} \n`);
                    }finally{
                        reject(err);
                    }
                }
            );
        });
    }

    getConnection(): Promise<typeof mongoose> {
        if (!connection){            
            Logger.info(TAG, `.${this.getConnection.name}  urlConnection = ${AppConfig.MONGODB.connectionString}`);
            let options: ConnectOptions = {                  
                poolSize: 1, // Maintain up socket connections defined in pool size appConfig.MONGODB.poolSize
                // If not connected, return errors immediately rather than waiting for reconnect
                bufferMaxEntries: 0,
                connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
                socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
                family: 4 // Use IPv4, skip trying IPv6
            };
            mongoose.set('useNewUrlParser', true);  //Set the global useNewUrlParser option to turn on useNewUrlParser for every connection by default.
            mongoose.set('useCreateIndex', true); //Set the useCreateIndex global option to opt in to making Mongoose use createIndex() instead
            /**
             * This options does not make sense when use "useUnifiedTopology" true:
             *  autoReconnect
             *  reconnectTries
             *  reconnectInterval
             */
            mongoose.set('useUnifiedTopology', true);                
            
            mongoose.set('autoIndex', AppConfig.MONGODB.autoIndex);   // when false, don't build indexes immediately
            mongoose.set('autoCreate', AppConfig.MONGODB.autoCreate); // When false, don't create collection until the first data trying to be inserted

            Logger.debug(TAG, `.${this.getConnection.name} mongoose.connect(${AppConfig.MONGODB.connectionString}, ${JSON.stringify(options, AppConfig.JSON.NO_REPLACER, AppConfig.JSON.TWO_SPACES)}`);
            connection = mongoose.connect(urlConnection, options);
        }
        return connection;
    }

    disconnect():Promise<void>{
        return mongoose.disconnect();
    }
}

const Repository = new RepositoryType();

export default Repository;