import {Document, model, PaginateModel, IPaginateResult, Schema, SchemaType} from 'mongoose'
import  {mongoosePagination} from './pagination/pagination';

import AppConfig from '../appConfig';
import PlaceDTO from '../dto/placeDTO';
import PlacePaginateResultDTO from '../dto/placePaginateResultDTO';
import Logger from '../util/logger';
import position, { Position, PositionSchema, POSITION_FIELD_MAP } from './position';

const TAG = 'place.ts';

export interface Place {    
    name: string;
    floor: number;
    number?: number;
    dimension: Position;    
}

 class PlaceFieldMap {    
    public NAME = "name";
    public FLOOR = "floor";
    public NUMBER = "number";
    public DIMENSION = "dimension";
    public dimensionMap = POSITION_FIELD_MAP;
}
export const PLACE_FIELD_MAP = new PlaceFieldMap();

export interface PlaceDocument extends Place, Document {
    fillModel(place: PlaceDTO | undefined) : void;
}
export interface PlaceModel extends PaginateModel<PlaceDocument> {
    exportData(rawData: PlaceDocument): PlaceDTO;
    transform(paginateResult: IPaginateResult<PlaceDocument>): PlacePaginateResultDTO;
}

const PlaceSchema = new Schema<PlaceDocument, PlaceModel>({    
    name: {
        type: String,
        required: [true, 'error.place.name.is.no.defined'],
        index: true
    },

    floor: {
        type: Number,       
        required: [true, 'error.place.floor.is.no.defined'],
    },

    number:{
        type: Number,
        required: false,
    },
    
    dimension: {
        type: PositionSchema,
        required: [true, 'error.place.dimension.is.no.defined']
    }
});

 PlaceSchema.methods.fillModel = function fillModel(placeDTO: PlaceDTO | undefined) : void{
    if (placeDTO?.name) this.set(PLACE_FIELD_MAP.NAME, placeDTO.name);
    if (placeDTO?.number) this.set(PLACE_FIELD_MAP.NUMBER, placeDTO?.number);
    if (placeDTO?.floor) this.set(PLACE_FIELD_MAP.FLOOR, placeDTO?.floor);
    if (placeDTO?.dimension) this.set(PLACE_FIELD_MAP.DIMENSION, placeDTO?.dimension);
}


PlaceSchema.statics.exportData = function exportData(rawData: PlaceDocument): PlaceDTO {
    try{
        Logger.debug(TAG, `.${PlaceSchema.statics.exportData.name} starting...`);        
    }finally{
        const result:PlaceDTO =  {
            id: rawData._id,
            name: rawData.name,
            floor: rawData.floor,
            number: rawData.number,
            dimension: rawData.dimension != undefined ? position.exportData(rawData.dimension) : undefined
        };
        Logger.debug(TAG, `.${PlaceSchema.statics.exportData.name} result = \n${JSON.stringify(result, AppConfig.JSON.NO_REPLACER, AppConfig.JSON.TWO_SPACES)}`);
        return result;
    }
}

PlaceSchema.statics.transform = function transform(paginateResult: IPaginateResult<PlaceDocument>): PlacePaginateResultDTO {
    const placeResult:PlacePaginateResultDTO = new PlacePaginateResultDTO().convert(paginateResult);
    return  placeResult;
}

PlaceSchema.plugin(mongoosePagination);
const  place = model<PlaceDocument, PlaceModel>('Place', PlaceSchema);
export default  place;