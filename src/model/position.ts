import {Document, Model, model,  Types, Schema} from 'mongoose';

const TAG = 'postition.ts';

export interface Position {
    x: number;
    y: number;
    z: number;
}

class PositionFieldMap {    
    public X = "x";
    public Y = "y";
    public Z = "z";
}
export const POSITION_FIELD_MAP = new PositionFieldMap();

export interface PositionDocument extends Position, Document {}
export interface PositionModel extends Model<PositionDocument> {
    exportData(rawData: any): Position
}

export const PositionSchema = new Schema<PositionDocument, PositionModel>({
    x: {
        type: Types.Decimal128,
        required: true,
        default: 0.000000
    },
    y: {
        type: Types.Decimal128,
        required: true,
        default: 0.000000
    },
    z: {
        type: Types.Decimal128,
        required: true,
        default: 0.000000
    }
});

PositionSchema.static('exportData', function exportData(rawData: PositionDocument): Position{    
    const result: Position = {
        x: parseFloat(rawData.x.toString()),
        y: parseFloat(rawData.y.toString()),
        z: parseFloat(rawData.z.toString())
    }
    return result;
});
 
export default model<PositionDocument, PositionModel>('Position', PositionSchema);