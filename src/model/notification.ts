import AppAssert from "../validation/appAssert";

export default class Notification {        
        public eventName: string;
        public data: object;
        constructor(eventName: string, data: object){
            new AppAssert(eventName != undefined && eventName.length > 0).orFail('error.notification.event.name.is.no.defined').verify();
            new AppAssert(data != undefined && Object.getOwnPropertyNames(data).length > 0).orFail('error.notification.data.is.no.defined').verify();
            this.data = {};
            Object.assign(this.data, data);
            this.eventName = eventName;
        }
    };



