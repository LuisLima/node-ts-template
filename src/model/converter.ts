export default  interface Converter<T, U>{
    convert(data: T): U
}