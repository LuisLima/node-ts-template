import {Document, model, PaginateModel, IPaginateResult, Schema} from 'mongoose';
import  {mongoosePagination} from './pagination/pagination';
import  jwt from 'jsonwebtoken';

import TokenDTO from '../dto/tokenDTO';
import TokenPaginateResultDTO from '../dto/tokenPaginateResultDTO';
import Logger from '../util/logger';
import AppConfig from '../appConfig';
import InvalidTokenException from '../exception/invalidTokenException';
import Bundle from '../i18n/bundle';

const TAG = 'token.ts';     

export interface Token{
    key: string;
    subject: string
    jsessionId: string
    algorithm: string
    issuedAt: string
    jwtid: string;
    expiryDate: number;
    notValidBefore: number;
}

class TokenFieldMap {
    public KEY = "key";
    public SUBJECT = "subject";
    public JSESSION_ID= "jsessionId";
    public ALGORITHM = "algorithm";
    public ISSUED_AT= "issuedAt";
    public JWT_ID =  "jwtid";
    public EXPIRY_DATE = "expiryDate";
    public NOT_VALID_BEFORE = "notValidBefore";
}
export const TOKEN_FIELD_MAP = new TokenFieldMap();

export interface TokenDocument extends Token, Document{  
    fillModel(token: string) :  Promise<Token>;  
    validateToken(token: string): Promise<Token>
}

export interface TokenModel extends PaginateModel<TokenDocument> {
    exportData(rawData: TokenDocument): TokenDTO;
    transform(paginateResult: IPaginateResult<TokenDocument>): TokenPaginateResultDTO;
    generateToken(subject: string, jsessionId: string): string;    
}
const TokenSchema = new Schema<TokenDocument, TokenModel>({        
    key: {
        type: String,
        required: [true, 'error.token.key.is.no.defined'],
        index: {
            unique:true
        }
    },
    subject: {
        type: String,
        required: [true, 'error.token.subject.is.no.defined'],
    },
    jsessionId: {
        type: String,
        required: [true, 'error.token.jsessionId.is.no.defined'],
    },
    algorithm: {
        type: String,
        required: [true, 'error.token.algorithm.is.no.defined'],
    },
    issuedAt: {
        type: String,
        required: [true, 'error.token.issuedAt.is.no.defined'],
    },
    jwtid: {
        type: String,
        required: [true, 'error.token.jwtid.is.no.defined'],
    },
    expiryDate: {
        type: Number,
        required: [true, 'error.token.expiryDate.is.no.defined'],
    },
    notValidBefore: {
        type: Number,
        required: [true, 'error.token.notValidBefore.is.no.defined'],
    }
});

TokenSchema.methods.fillModel = function fillModel(token: string) : Promise<Token> {
    const modelInstance = this;
    return new Promise(async (resolve, reject) =>{
        const tokenModel: Token | void = await this.validateToken(token).catch(err => reject(err));    
        if (tokenModel){
            modelInstance.key = tokenModel.key;
            modelInstance.subject = tokenModel.subject;
            modelInstance.jsessionId = tokenModel.jsessionId;
            modelInstance.algorithm = tokenModel.algorithm;
            modelInstance.issuedAt = tokenModel.issuedAt;
            modelInstance.jwtid = tokenModel.jwtid;
            modelInstance.expiryDate = tokenModel.expiryDate;
            modelInstance.notValidBefore = tokenModel.notValidBefore;
            resolve(modelInstance);
        }else{
            reject();
        }
    });
}

TokenSchema.statics.exportData = function exportData(rawData: TokenDocument): TokenDTO {
    try{
        Logger.debug(TAG, `.${TokenSchema.statics.exportData.name} starting...`);        
    }finally{
        const result: TokenDTO =  {
            id: rawData._id,
            key: rawData.key,
            subject: rawData.subject,
            jsessionId: rawData.jsessionId,
            algorithm: rawData.algorithm,
            issuedAt: rawData.issuedAt,
            jwtid: rawData.jwtid,
            expiryDate: rawData.expiryDate,
            notValidBefore: rawData.notValidBefore
        };
        Logger.debug(TAG, `.${TokenSchema.statics.exportData.name} result = \n${JSON.stringify(result, AppConfig.JSON.NO_REPLACER, AppConfig.JSON.TWO_SPACES)}`);
        return result;
    }
}    

TokenSchema.statics.transform = function transform(paginateResult: IPaginateResult<TokenDocument>): TokenPaginateResultDTO {
    const tokenResult: TokenPaginateResultDTO = new TokenPaginateResultDTO().convert(paginateResult);
    return  tokenResult;
}

TokenSchema.statics.generateToken = function generateToken(subject: string, jsessionId: string): string {
    return jwt.sign(
        {subject},  // This is the payload we want to put inside the token
        AppConfig.TOKEN.SECRET, // Secret string which will be used to sign the token,
        {
            subject: subject,
            audience: jsessionId,
            algorithm:  AppConfig.TOKEN.ALGORITHM,
            expiresIn: AppConfig.TOKEN.MAX_AGE,
            issuer: AppConfig.TOKEN.ISSUER,
            jwtid: AppConfig.TOKEN.KEY,
            keyid: AppConfig.TOKEN.KEY
        }
    )    
};


/**
* Validate the token based in token an current user session.
* @param token the token to be validated        
*  return a map containing all token fields
*/
TokenSchema.methods.validateToken = function validateToken(token: string): Promise<Token> {
    const modelInstance = this;
    return new Promise((resolve, reject) =>{
        jwt.verify(token, AppConfig.TOKEN.SECRET, function(err, decoded){
            if (err) reject(new InvalidTokenException(err.message, err.stack));
            else {
                if (decoded){
                    Logger.debug(TAG, `.${modelInstance.validateToken.name} ${JSON.stringify(decoded, AppConfig.JSON.NO_REPLACER, AppConfig.JSON.TWO_SPACES)}`);
                    const tokenModel: Token = {
                        key: token,
                        subject: decoded["sub" as keyof object],
                        jsessionId: decoded["aud" as keyof object],
                        algorithm: AppConfig.TOKEN.ALGORITHM,
                        issuedAt: decoded["iss" as keyof object],
                        jwtid: decoded["jti" as keyof object],
                        expiryDate: decoded["exp" as keyof object],
                        notValidBefore: decoded["iat" as keyof object]
                    }
                    resolve(tokenModel);
                }else{
                    reject(new InvalidTokenException(Bundle.key("error.token.decoded.object.no.defined").get()));
                }
            }
        })
    })
};
    
TokenSchema.plugin(mongoosePagination);
const  tokenModel = model<TokenDocument, TokenModel>('Token', TokenSchema);
export default  tokenModel;

