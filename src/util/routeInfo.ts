
import * as express from "express";
import Logger from "./logger";
class RouteInfoType {
    private  TAG = RouteInfoType.name;       
    getRoutesPaths(app: express.Application): string[]  {
        Logger.debug(this.TAG, `.${this.getRoutesPaths.name} starting`);  
        const paths:string[] = [];
        app._router.stack.forEach( (property:any) => {
            if (property.route && property.route.path){
                const method = property.route.methods ? (Object.getOwnPropertyNames(property.route.methods).pop() as string).toUpperCase() : "GET";
                paths.push(`${method} ${property.route.path}`);
            }
        });
          return paths;
    }

    printRoutes (app: express.Application) {
        this.getRoutesPaths(app).forEach(path => Logger.debug(this.TAG, path));
        
    };   
}

const RouteInfo = new RouteInfoType();
export default RouteInfo;