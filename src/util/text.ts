class TextType {
    uppercaseNormalizedText(text: string){
        if (!text) return "";
        return text.toUpperCase()
        .normalize("NFD") //NFD Unicode normal form decomposes combined graphemes into the combination of simple ones
        .replace(/[\u0300-\u036f]/g, "") //replace accented characters 
    }

    lowercaseNormalizedText(text: string){
        if (!text) return "";
        return text.toLowerCase()
        .normalize("NFD") //NFD Unicode normal form decomposes combined graphemes into the combination of simple ones
        .replace(/[\u0300-\u036f]/g, "") //replace accented characters 
    }        

    capitalizeFirstLetter(text: string) {
        return text[0].toUpperCase() + text.slice(1);
    }

    padZeros(num: number, size: number) {
        var s = "000000000" + num;
        return s.substr(s.length-size);
    }    

    padRightSpaces(value: string, size: number) {
        var s =  value + "     " ;
        return s.substr(0, size);
    }
    
    isEmpty(value: string | undefined | null): boolean {        
        return value == undefined || value == null || value == "" ;
    }

}

const Text = new TextType();
export default Text;