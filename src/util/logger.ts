import dateFormat from "dateformat";
import AppConfig from "../appConfig";
import BusinessException from "../exception/businessException";
import Bundle from "../i18n/bundle";
import Text from "./text";

interface ILevelData {
     INFO: string;
     ERROR: string;
     DEBUG: string;
}

class LevelType {
    public INFO: string = "INFO"
    public ERROR: string = "ERROR";
    public DEBUG: string = "DEBUG";
    isValid(level: string): boolean {
        let normalizedLevel = Text.uppercaseNormalizedText(level);
        return Object.getOwnPropertyNames(this).indexOf(normalizedLevel) > -1  ;
    }
    exportData(): ILevelData {
        return {
            INFO: this.INFO,
            ERROR: this.ERROR,
            DEBUG: this.DEBUG
        };              
    }
};
const Level = new LevelType();


interface ILogData{
    tag: string;
    text: string;
    level: string;
    timestamp: Date;
    result: string;    
}

/**
 * 
 * @param {string} tag - o nome da operação ou objeto que se deseja identificar a geração do log
 * @param {string} text  - a mensagem de texto que se deseja imprimir.
 * @param {string} level  - o nível de log que se deseja definir:
 *                          INFO - apenas mensagens informativas.          
 *                          ERROR - apenas mensagens de erro serão impressas 
 *                          DEBUG - todas as mensagens serão impressas
 */
class Log {
    TAG = Log.name;
    public tag: string;
    public text: string;
    public level: string;
    public timestamp: Date;
    public result: string;
    private LEVEL_SIZE: number = 5;
    constructor (tag: string, text: string, level: string){
        this.tag = tag;
        this.text = text;
        this.level = Level.isValid(level) ? level :  Level.INFO;
        this.timestamp = new Date();
        this.result = `[${Text.padRightSpaces(this.level, this.LEVEL_SIZE)}] ${dateFormat(this.timestamp, "isoDateTime")}: ${this.tag} ${this.text}`;
    }

    private _notifyLog = () => {
        // let isSafe = app && app.common && app.common.eventNames && app.model && app.model.notification && app.service && app.service.notificationService;
        // if (isSafe){
        //     const EVENT_NAMES = app.common.eventNames;
        //     const NotificationModel = app.model.notification;
        //     let data = {"log": self.exportData()};
        //     let notification = new NotificationModel(EVENT_NAMES["LOG_MESSAGE"], data);                    
        //     app.service.notificationService.notify(notification);         
        // }
        console.log(`[${Text.padRightSpaces(this.level, this.LEVEL_SIZE)}] ${dateFormat(this.timestamp, "isoDateTime")}: ${this.TAG} ${this._notifyLog.name} NEEDS IMPLEMENTATION!`)
    };  

    private _getLogFunction = (level: string): Function => {
        let  functionName = level.toLowerCase()
        try {
            if (typeof console[functionName as keyof object] == "function") return console[functionName as keyof object];
            else return console.log;
        } catch (err) {
            console.error(`${dateFormat(new Date(), "isoDateTime")}: ${this.TAG}.${this._getLogFunction.name}  ${err}`);
            return console.log;
        }
    };

    print(notify: boolean = true): void {
        const logger = Logger;        
        if (this.level == logger._currentLevel || 
            logger._currentLevel == Level.DEBUG ||
            this.level == Level.ERROR){
            let log = this._getLogFunction(this.level);
            log(this.result);
        }
        if (logger._isLogToBeNotified && notify) this._notifyLog();
    };

    exportData(): ILogData {
        return {
                tag: this.tag,
                text: this.text,
                level: this.level,
                timestamp: this.timestamp,
                result: this.result
        }; 
    };
};

interface ILoggerData {
    currentLevel: string
    isLogToBeNotified: boolean
    level: ILevelData
}
interface ILogConfig{
    logConfig: ILoggerData    
}

class LoggerType {
    TAG = LoggerType.name;
    constructor(){
        console.log(`|---\n${dateFormat(new Date(), "isoDateTime")}: ${this.TAG} new instance\n---|\n`);
    }
    
    _currentLevel = AppConfig.LOG.level;
    _isLogToBeNotified = AppConfig.LOG.isLogToBeNotified;        
    private _notifyLogConfigUpdate = () => {
        // const EVENT_NAMES = app.common.eventNames;            
        // const NotificationModel = app.model.notification;
        // let data = {"logConfig": this.exportData()};
        // let notification = new NotificationModel(EVENT_NAMES["LOG_CONFIG_UPDATE"], data);                    
        // app.service.notificationService.notify(notification);         
    };

    private _print(tag: string, text: string, level: string, notify: boolean = true){
        try{
            if (!tag) throw new Error(Bundle.key('error.log.tag.is.no.defined').get());
            if (!text) throw new Error(Bundle.key('error.log.text.is.no.defined').get());
            new Log(tag, text, level).print(notify);
        }catch(e){
            const err = e as Error;
            console.error(`${dateFormat(new Date(), "isoDateTime")}: ${this.TAG} \n${err.message}\nstack: ${err.stack}`);
        }
    };

    info(tag: string, text: string, notify: boolean = true){
        this._print(tag, text, Level.INFO, notify);
    };

    error(tag: string, text: string, notify: boolean = true){
        this._print(tag, text, Level.ERROR, notify);
    };

    debug(tag: string, text: string, notify: boolean = true){
        this._print(tag, text, Level.DEBUG, notify);
    };

    defineInfo(){
        this._currentLevel = Level.INFO;
        this._notifyLogConfigUpdate();
    };        

    defineError(){
        this._currentLevel = Level.ERROR;
        this._notifyLogConfigUpdate();
    };

    defineDebug(){
        this._currentLevel = Level.DEBUG;
        this._notifyLogConfigUpdate();
        };

    isLevelInfo(){
        return this._currentLevel == Level.INFO;
    };

    isLevelDebug(){
        return this._currentLevel == Level.DEBUG;
    };
    isLevelError(){
        return this._currentLevel == Level.ERROR;
    };

    defineLevel(name: string): ILogConfig{
        if (name && Level.isValid(name)){
            
            let normalizedNameLevel = Text.lowercaseNormalizedText(name);
            const defineMethodName = "define" + Text.capitalizeFirstLetter(normalizedNameLevel);
            Object.call(this, defineMethodName);
            
            let result = {logConfig: this.exportData()};
            return result;
        }else{
            throw new BusinessException(Bundle.key('error.log.level.is.not.found').get())
        }
        };

        enableNotificationMessage(){
            this._isLogToBeNotified = true;
            this._notifyLogConfigUpdate();
        };

        disableNotificationMessage(){
            this._isLogToBeNotified = false;
            this._notifyLogConfigUpdate();
        };

        exportData(): ILoggerData {
        return {
            currentLevel: this._currentLevel,
            isLogToBeNotified: this._isLogToBeNotified,
            level: Level.exportData()
        }; 
        
    };       
};

let Logger = new LoggerType();
Logger.info(LoggerType.name, `Initial Config: ${JSON.stringify(Logger.exportData(), AppConfig.JSON.NO_REPLACER, AppConfig.JSON.TWO_SPACES)}`);    

export default Logger;
