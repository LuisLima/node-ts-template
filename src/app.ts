import express from 'express';
import AppConfig from './appConfig';
import * as middleware from './middleware/middleware';
import Respository from './persistence/repository';
import * as route from './route/route';
import NotificationService from './service/notificationService';
import Logger from './util/logger';

const app = express();
const TAG = app.name;
const server = require('http').Server(app);
app.use(express.json());
app.use("/public", express.static('public')); 

//Init MiddleWares
middleware.register(app);

//Init connection pool 
Respository.start();

//Init Routes
route.register(app);

server.listen(AppConfig.PORT, () => {
    Logger.info(TAG, `⚡️[server]: Server is running at https://localhost:${AppConfig.PORT}`);
})

//Init websocket notifier service
NotificationService.start(server);
