import packageJson from "../../package.json";
import Logger from "../util/logger";

class PlaceEventNamesType {
    private TAG: string = PlaceEventNamesType.name;
    readonly getPlaceEvent = `${packageJson.name}.getPlaceEvent`;
    constructor(){
        Logger.debug(this.TAG, `Properties: `);
        
        const propertyNames = Object.getOwnPropertyNames(this);
        propertyNames.forEach((name, index) => 
            Logger.debug(this.TAG, `${name} = ${this[name as keyof object]}${ index == propertyNames.length -1 ? '\n' : ''}`)
        );
    }
}
export const PlaceEventNames = new PlaceEventNamesType();
