import { Request } from "express";

export default class Context{

    static _bindings = new WeakMap<Request, Context>()

    public  dataMap: {[k: string]: number | string | boolean | Object | Array<any>} = {};

    constructor() {}
    
    static bind(req: Request): void {
        const context = new Context();
        Context._bindings.set(req, context);
    }

    static get(req: Request): Context | null {
        return Context._bindings.get(req) || null;
    }
}