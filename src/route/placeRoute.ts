import * as express from "express";
import { StatusCodes } from "http-status-codes";
import PlaceDTO from "../dto/placeDTO";
import { PlaceEventNames } from "../event";
import Bundle from "../i18n/bundle";
import NotificationService from "../service/notificationService";
import PlaceService from "../service/placeService";
import Logger from "../util/logger";
import Url from "./url";

export const register = (app: express.Application) => {    
    const TAG = 'placeRoute';       
    Logger.debug(TAG, `.${register.name} starting`);
    
    app.get(`${Url.ROUTE_PLACE}`, async (req, res) =>{    
        /*  #swagger.path = '/api/place'
            #swagger.security = [{
                "api_key": []
            }]
            #swagger.parameters['page'] = {
                in: 'query',
                required: false,
                type: 'integer',
                description: 'select page number.',
            }
            #swagger.parameters['limit'] = {
                in: 'query',
                required: false,
                type: 'integer',
                description: 'select page limit.',
            } 
        */               
        try{
            const filter:PlaceDTO = {
                floor: req.query.floor as string != undefined ? parseInt(req.query.floor as string) : undefined 
            };
            const page: number | undefined = req.query.page as string != undefined ? parseInt(req.query.page as string) : undefined;
            const limit: number | undefined = req.query.limit as string != undefined ? parseInt(req.query.limit as string) : undefined;
            const place = await PlaceService.findByFilter(filter, page, limit).catch((err:Error) => {                
                Logger.error(TAG, `.${PlaceService.findByFilter.name} message: ${err.message}\n stack: ${err.stack}`);
                const errorResult = {message: err.message, stack: err.stack};
                /* #swagger.responses[500] = { 
                        description: 'internal server failure.',
                        schema: { $ref: '#/definitions/internalError' }
                } */                
                res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(errorResult);
            });
            try{
                NotificationService.notify({eventName: PlaceEventNames.getPlaceEvent, data: place || {}}); //Only for test purposes. Delete when is not needed! 
            }catch(e){
                const err = e as Error;
                Logger.error(TAG, err.message);
            }

            /* #swagger.responses[200] = {
                    description: 'return the first 20 places.',
                    schema: { $ref: '#/definitions/getPlaces' }
            } */               
            res.status(StatusCodes.OK).json(place);
        }catch(err){
            /**
                 #swagger.responses[500] = { 
                    description: 'internal server failure.',
                    schema: { $ref: '#/definitions/internalError' }
            } */             
            res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(err);
        }
    });    


    app.get(`${Url.ROUTE_PLACE}${Url.ROUTE_PLACE_ID}`, async (req, res) => {    
        /*  #swagger.path = '/api/place/{placeId}'
            #swagger.security = [{
                "api_key": []
            }]
            #swagger.parameters['placeId'] = {
                description: 'place id.',
                in: 'path',
                required: true,
                type: 'string'
            } 
        */       
        try{
            const place = await PlaceService.findById(req.params[Url.ROUTE_PLACE_ID_PARAMETER]).catch((err:Error) => {
                Logger.error(TAG, `.${PlaceService.findById.name} message: ${err.message}\n stack: ${err.stack}`);
                const errorResult = {message: err.message, stack: err.stack};
                /* #swagger.responses[500] = { 
                        description: 'internal server failure.',
                        schema: { $ref: '#/definitions/internalError' }
                } */
                res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(errorResult);
            });
            if (place){
                /*  #swagger.responses[200] = {
                        description: 'return desired place.',
                        schema: { $ref: '#/definitions/getPlaceById' }
                } */                
                res.status(StatusCodes.OK).json(place);
            }else{
                /*  #swagger.responses[401] = { 
                        description: 'place not found.',
                        schema: { $ref: '#/definitions/internalError' }
                } */
                res.status(StatusCodes.NOT_FOUND).json(Bundle.key('error.place.id.is.not.found').get());
            }
        }catch(err){
            /*  #swagger.responses[500] = { 
                    description: 'internal server failure.',
                    schema: { $ref: '#/definitions/internalError' }
            } */            
            res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(err);
        }
    });
    
    app.post(Url.ROUTE_PLACE, async (req, res) =>{
        /* #swagger.path = '/api/place'
            #swagger.security = [{
                "api_key": []
            }]        
            #swagger.parameters['body'] = {
                in: 'body',
                description: 'add Place.',            
                schema: { $ref: '#/definitions/addPlace' }
        } */        
        try{
            
            const placeData = req.body.place;
            if (!placeData) return res.status(StatusCodes.BAD_GATEWAY).send(Bundle.key('error.place.is.no.defined').get());
            let place: PlaceDTO = {
                name: placeData.name,
                floor: placeData.floor,
                number: placeData.number,
                dimension: placeData.dimension
            };
            const placeSaved = await PlaceService.save(place).catch((err:Error) => {
                Logger.error(TAG, `.${PlaceService.save.name} message: ${err.message}\n stack: ${err.stack}`);
                const errorResult = {message: err.message, stack: err.stack};
                /*  #swagger.responses[500] = { 
                        description: 'server failure.',
                        schema: { $ref: '#/definitions/internalError' }
                } */
                res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(errorResult);
            });
            
            /*  #swagger.responses[201] = {
                    description: 'new place saved.',
                    schema: { $ref: '#/definitions/addPlaceResult' }
            } */
            res.status(StatusCodes.CREATED).json(placeSaved);
            
        }catch(err){
            /* #swagger.responses[500] = { 
                    description: 'server failure.',
                    schema: { $ref: '#/definitions/internalError' }
            } */
            res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(err);
        }
    });
    
    app.put(`${Url.ROUTE_PLACE}${Url.ROUTE_PLACE_ID}`, async (req, res) =>{
        /*  #swagger.path = '/api/place/{placeId}'
            #swagger.security = [{
                "api_key": []
            }]
            
            #swagger.parameters['placeId'] = {
                description: 'place id.',
                in: 'path',
                required: true,
                type: 'string'
            }                        
            #swagger.parameters['body'] = {
                in: 'body',
                description: 'update place.',            
                schema: { $ref: '#/definitions/updatePlace' }
        } */
        try{
            
            const placeData = req.body.place;
            if (!placeData) {
                /*  #swagger.responses[502] = { 
                        description: 'place object not defined .',
                        schema: '[i18n] error.place.is.no.defined'
                } */
                return res.status(StatusCodes.BAD_GATEWAY).send(Bundle.key('error.place.is.no.defined').get());
            }
            let place: PlaceDTO = {
                id: req.params[Url.ROUTE_PLACE_ID_PARAMETER],
                name: placeData.name,
                floor: placeData.floor,
                number: placeData.number,
                dimension: placeData.dimension
            };
            const placeSaved = await PlaceService.save(place).catch((err:Error) => {
                Logger.error(TAG, `.${PlaceService.save.name} message: ${err.message}\n stack: ${err.stack}`);
                const errorResult = {message: err.message, stack: err.stack};
                if (Bundle.key("error.place.id.is.not.found").get() === errorResult.message){
                    /*  #swagger.responses[401] = { 
                            description: 'place not found.',
                            schema: { $ref: '#/definitions/internalError' }
                    } */
                    res.status(StatusCodes.NOT_FOUND).json(errorResult);
                }else{
                    /*  #swagger.responses[500] = { 
                            description: 'internal server failure.',
                            schema: { $ref: '#/definitions/internalError' }
                    } */
                    res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(errorResult);
                }
            });
            
            /*  #swagger.responses[200] = {
                    description: ' existent place updated.',
                    schema: { $ref: '#/definitions/updatePlaceResult' }
            } */
            res.status(StatusCodes.OK).json(placeSaved);
            
        }catch(err){
            /*  #swagger.responses[500] = { 
                    description: 'server failure.',
                    schema: { $ref: '#/definitions/internalError' }
            } */
            res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(err);
        }
    });    
};