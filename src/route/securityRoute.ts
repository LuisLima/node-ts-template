import * as express from "express";
import { StatusCodes } from "http-status-codes";
import AuthenticationDTO from "../dto/autenticationDTO";
import Bundle from "../i18n/bundle";
import SecurityService from "../service/securityService";
import Logger from "../util/logger";
import Url from "./url";

export const register = (app: express.Application) => {    
    const TAG = 'securityRoute';       
    Logger.debug(TAG, `.${register.name} starting`);     

    
    app.post(Url.ROUTE_SECURITY_LOGON, async (req, res) =>{
        /*  #swagger.path = '/api/security/logon'
            #swagger.parameters['body'] = {
                in: 'body',
                description: 'logon.',            
                schema: { $ref: '#/definitions/logon' }
        } */
        try {
            const authenticationData = req.body.authenticationData;
            if (!authenticationData) return res.status(StatusCodes.BAD_REQUEST).send(Bundle.key('error.authentication.data.is.no.defined').get());
            const authDTO: AuthenticationDTO = {
                username: authenticationData.username,
                password: authenticationData.password
            }
            const token = await SecurityService.authenticate(authDTO, req.hostname).catch((err:Error) => {
                Logger.error(TAG, `.${SecurityService.authenticate.name} message: ${err.message}\n stack: ${err.stack}`);
                const errorResult = {message: err.message, stack: err.stack};
                /* #swagger.responses[500] = { 
                        description: 'server failure.',
                        schema: { $ref: '#/definitions/internalError' }
                } */
                res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(errorResult);
            });
            
            /* #swagger.responses[200] = {
                    description: 'string containing the token.',
                    schema: '1rBhbGcPoIJIUzI1AiIsInQ5cCI6IkpXVCIsImtpZCI6IjQ4OWJhNmRlLWVkM2UtNGJkZi1hNWJmLWRjYWQ3Nzg4NjA0MyJ9.eyJzdWJqZWN0IjoiYWRtaW4iLCJpYXQiOjE3MDQyMDEwMjcsImV4cCI6MTcwNDUwMTAyNywiYXVkIjoibG9jYWxob3N0IiwiaXNzIjoiYXBwOmlzc3VlciIsInN1YiI6ImFkbWluIiwianRpIjoiNDg5YmE2ZGUtZWQzZS00YmRmLWE1YmYtZGNhZDc3ODg2MDQzIn0.t7eCz5fAsa2tkL02tbiZI2J_1-vlIlDXe9wAEGRXk8Y'
            } */            
            res.status(StatusCodes.OK).json(token);
            
        }catch(err){
            /*   #swagger.responses[500] = { 
                    description: 'server failure.',
                    schema: { $ref: '#/definitions/internalError' }
            } */
            res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(err);
        }
    });    
};