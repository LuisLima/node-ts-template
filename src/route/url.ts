class UrlType{
    public BASE_ROUTE_API = "/api";
    
    public API_DOC = `${this.BASE_ROUTE_API}/doc`;
    public ROUTE_PLACE = `${this.BASE_ROUTE_API}/place`;
    public ROUTE_PLACE_ID_PARAMETER = "placeId";
    public ROUTE_PLACE_ID = `/:${this.ROUTE_PLACE_ID_PARAMETER}`;

    public ROUTE_SECURITY = `${this.BASE_ROUTE_API}/security`;
    public ROUTE_LOGON = "/logon";
    public ROUTE_SECURITY_LOGON = `${this.ROUTE_SECURITY}${this.ROUTE_LOGON}`;

}
const Url = new UrlType();
export default Url;