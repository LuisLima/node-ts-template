import * as express from "express";
import packageJson from '../../package.json';
import AppConfig from "../appConfig";
import Logger from "../util/logger";
import RouteInfo from "../util/routeInfo";
import url from './url';
import Url from "./url";
import Bundle from "../i18n/bundle";

export const register = (app: express.Application) => {    
    const TAG = 'rootRoute';       
    Logger.debug(TAG, `.${register.name} starting`);     

    const render = (routes: string[]) => {
        const content = `
            <!DOCTYPE html>
            <html>
                <head>
                    <link rel="icon" href="/public/favicon.ico">
                    <style>
                        .title {
                            text-transform: capitalize
                        }
                    </style>
                </head>            
                <h1>${packageJson.name}API Routes</h1>
                <div>
                    <h3 class="title"><a href='${Url.API_DOC}'>${Bundle.key('info.api.documentation').get()}</a><h3>
                    <pre>
                        ${JSON.stringify(routes, AppConfig.JSON.NO_REPLACER, AppConfig.JSON.TWO_SPACES)}
                    </pre>
                </div>
            </html>
        `;
        return content;
    }

    app.get('/', (req, res) => res.send(render(RouteInfo.getRoutesPaths(app))));
    
    // #swagger.path = '/api'
    app.get(url.BASE_ROUTE_API, (req, res) => res.send(render(RouteInfo.getRoutesPaths(app))));
};