import * as express from "express";
import Logger from "../util/logger";
import RouteInfo from "../util/routeInfo";
import * as placeRoute from './placeRoute';
import * as rootRoute from './rootRoute';
import * as securityRoute from './securityRoute';
import Url from "./url";
import swaggerUi from "swagger-ui-express";
import swaggerOutput from "../swagger_output.json";
import Bundle from "../i18n/bundle";
export const register = (app: express.Application) => {
    const TAG = 'route';       
    Logger.debug(TAG, `.${register.name} starting`);  
    
    rootRoute.register(app);
    securityRoute.register(app);
    placeRoute.register(app);
    
    //#swagger.path = '/api/doc'
    app.use(Url.API_DOC, swaggerUi.serve, swaggerUi.setup(swaggerOutput));

    //print registered routes    
    Logger.debug(`\n\n\t[${TAG}`, `.${register.name}]\n`);    
    app._router.stack.forEach( (property:any) =>{        
        if (property?.route?.path){
            const method = property.route.methods ? (Object.getOwnPropertyNames(property.route.methods).pop() as string).toUpperCase() : "GET";
            Logger.debug(TAG, `${method} ${property.route.path}`);
        }
      });    
};