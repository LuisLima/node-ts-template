import Context from "../route/Context";
import * as messageEnUS from "./message.json";
import * as messagePtBR from "./message-pt_BR.json";
import { Request } from "express";
import AppConfig from "../appConfig";
import Logger from "../util/logger";
const _getKey = function _getKey(bundleMessages: object, key:string | undefined){
    if (!bundleMessages || !key) return undefined;
    return Object.getOwnPropertyNames(bundleMessages).find(function(msg) {
        return typeof msg !== "function" && msg === key;
    });
}
class MessageBuilder{
    TAG: string = MessageBuilder.name;
    private _key: string | undefined;    
    private _locale: string | undefined;
    private _replacerList: any[] = [];
    
    constructor(){}
    

    key(key:string): MessageBuilder {
        this._key = key;
        return this;
    }

    locale(locale:string): MessageBuilder {
        this._locale = locale;
        return this;
    }

    replacerList(replacerList: any[]){
        if (replacerList && replacerList.length > 0){
            this._replacerList = this._replacerList.concat(replacerList);
        }
        return this;
    };    

    get(){
        let messageResult = "";
        try{
            this._locale = this._locale || this.getLocaleFromContext();
            let message = Bundle["pt_BR"];
            if (this._locale && Bundle[this._locale as keyof object]) message = Bundle[this._locale as keyof object];
            let messageKey = _getKey(message, this._key);
            if (!message[messageKey as keyof object]) throw new Error(message["error.message.not.found"].replace('{1}', messageKey || ""));
            messageResult = message[messageKey as keyof object];
            if (this._replacerList && this._replacerList.length > 0){
                this._replacerList.forEach(function(repl, index){
                    messageResult = messageResult.replace("{"+(index + 1)+"}", repl);
                }); 
            }
        }catch(e){
            const err = e as Error;
            Logger.error(this.TAG,`.${this.get.name} ${err.message}\nstack: ${err.stack}`);
        }finally{
            return messageResult;
        }        
    }

    private getLocaleFromContext(): string{
        let contextLocale: string = 'pt_BR';
        try{
            const context = Bundle.getContext();
            if (context && context.dataMap["locale"]){
                contextLocale =  context.dataMap["locale"] as string;
                contextLocale = contextLocale.replace("-", "_");
            }
        }catch(e){
            const err = e as Error;
            Logger.error(this.TAG,`.${this.getLocaleFromContext.name} ${err.message}\nstack: ${err.stack}`);
        }finally{
            return contextLocale;
        }
    }    
}


class BundleType {
    TAG: string = BundleType.name;
    private request: Request | null = null;
    en_US = messageEnUS;
    pt_BR = messagePtBR;

    constructor() {}

    setRequest(request: Request){ 
        if (request) {
            this.request = request;
            Logger.debug(this.TAG,`.${this.setRequest.name} ${JSON.stringify(this.request.path, AppConfig.JSON.NO_REPLACER, AppConfig.JSON.TWO_SPACES)}`);
        }
    }
    getContext():Context | null {
        if (this.request){
            return Context.get(this.request);
        }
        return null ;
    }

    key(key: string){
        if (!key) throw Error(this.en_US["error.message.is.no.defined"]);
        return new MessageBuilder().key(key);
    }

    has(key: string){
        if (!key) return false;
        return _getKey(this.en_US, key) != undefined;
    };    
}

const Bundle = new BundleType();
export default Bundle;