#variáveis de ambiente

setx NODE_ENV "development"
setx NODE_HOME "D:\Development\nodejs"
setx GIT_HOME "D:\Development\Git"
setx MONGODB_HOME "D:\Development\MongoDB"

setx SERVER_PORT 3002

setx TASK_TIMEOUT 1

setx LOG_LEVEL "DEBUG"
setx LOG_IS_TO_BE_NOTIFIED false

setx MONGODB_PASS "test"
setx MONGODB_USER "test"
setx MONGODB_CONN "mongodb://{{username}}:{{password}}@localhost:27017/test"

pause