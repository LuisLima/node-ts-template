/**
 *  This script is used to replace secrets in a file with the actual values.
 *  Following are the parameters that can be passed to the script:
 *      --filePath: (required) the path to the file that contains the secrets
 * 
 *      -e: (optional) encode the secret value to base64. if you not pass this parameter,
 *          the secret value will be replaced as is.
 *      
 *      --secretName: (required) the name of the secret to replace. you can pass multiple
 *                    secret names to replace. for example:
 * 
 *          --MONGO_ROOT_USERNAME_VALUE=admin 
 *          --MONGO_USERS_LIST_VALUE=test:dbAdmin,readWrite:test
 * 
 *   Following is an example of how to use this script:
 *      node secret-replacer.js -e
 *      --filePath=./secret.yml \
 *      --MONGO_USERNAME_VALUE=YOUR_USER \
 *      --MONGO_PASSWORD_VALUE=YOUR_PASSWORD \
 *      --MONGO_CONN_VALUE=YOUR_DATABASE_CONNECTION_STRING  
 */

const {readFile, writeFile} = require('fs');
function getArgs () {
    const args = {};
    process.argv
        .slice(2, process.argv.length)
        .forEach( arg => {
        // long arg
        if (arg.slice(0,2) === '--') {
            const longArg = [arg.slice(0, arg.indexOf('=')), arg.slice(arg.indexOf('=')+1)];            
            const longArgFlag = longArg[0].slice(2,longArg[0].length);
            const longArgValue = longArg.length > 1 ? longArg[1] : true;
            args[longArgFlag] = longArgValue;
        }
        // flags
        else if (arg[0] === '-') {
            const flags = arg.slice(1,arg.length).split('');
            flags.forEach(flag => {
            args[flag] = true;
            });
        }
    });
    
    return args;
}
const argMap = getArgs();

function replaceSecret (argMap){
    const filePath = argMap['filePath'];
    const encode = argMap['e'] != undefined ? argMap['e'] : false;
    if (!filePath) {
        console.log("No 'filePath' argument provided"); 
        return;
    }

    if (argMap.length < 2)  {
        console.log("No 'secretName' argument provided"); 
        return;    
    }
    readFile(filePath, 'utf-8', function (err, contents) {
        // console.log(`Original contents=\n${contents}`);
        if (err) {
          console.log(err);
          return;
        }
        let contentReplaced = contents;
        Object.getOwnPropertyNames(argMap)
            .filter(name => name !== 'filePath' && name !== 'e')
            .forEach(secretName => {
            const pattern = `${secretName}`;
            const regex = new RegExp(pattern, 'g');
            // console.log(`test pattern = ${pattern} result in:  ${regex.test(contents)}`)
            contentReplaced = contentReplaced.replace(regex, encode ? Buffer.from(argMap[secretName]).toString('base64') : argMap[secretName]);
        });

        writeFile(filePath, contentReplaced, 'utf-8', function (err) {
          if (err) console.log(`Error: ${JSON.stringify(err, null, 2)}`);
          else{
                console.log(`File ${filePath} was updated`);            
          }
        });
        // console.log(`Replaced contents=\n${contentReplaced}`);
      });
}
replaceSecret(argMap);
console.log(argMap);