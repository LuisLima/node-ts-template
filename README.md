#  Node TypeScript Template

Esse projeto é um template de aplicação que destina-se a definir um conjunto básico de funcionalidades iniciais em NodeJS com typeScript.
As principais funcionalidades implementadas são:

* Internacionalização (i18n) - Português e Inglês
* Acesso ao banco de dados MongoDB
* Registro global de middlewares 
* Api Rest com Express
* Comunicação via WebSocket 
* Gerenciamento de Log
* Api de validação 
* Serviço básico de geração de token para autenticação

## Scripts disponíveis (Diretório Raiz)

### **Execução via container**
Caso deseje usar o docker para executar a aplicação, utilize as imagens disponíveis no DockerHub [node-ts-template](https://hub.docker.com/repository/docker/lggugs/node-ts-template) e [mongo](https://hub.docker.com/repository/docker/lggugs/template-mongodb) e execute-as em seu ambiente da seguinte forma:  

1. Crie a network **"template-bridge"** para comunicação entre os containers:

    ```bash
    docker network create --driver bridge template-bridge
    ```

2. Crie o volume **"template-volume"** para persistência dos dados do MongoDB:  

    ```bash
    docker volume create template-volume
    ```
3. Execute os containers na seguinte ordem:  

    ```bash
    docker run --name mongodb -d --network template-bridge -p 27017:27017 -v template-volume:/data/db lggugs/template-mongodb:1.0
    docker run -d --name node-ts-template --network template-bridge -p 3000:3000 lggugs/node-ts-template:1.0
    ```  
4. Abra o navegador e digite o endereço [http://localhost:3000](http://localhost:3000) ou endereço da máquina que está executando o container.  
    
### **Execução local**  

1. **Instalar as dependências do projeto**:
    ```bash
    npm install
    ```
 
2. **Configurar MongoDB Localmente:**  

    1. Download do [MongoDB](https://www.mongodb.com/try/download/community)  
    
    2. Executar o mongodb Standalone no terminal sem autenticação 
        ```go
        <MONGODB_PATH>\bin\mongod --port 27017 --dbpath <YOUR_PATH>\mongodb_data\db
        ``` 

    3. Criar usuário "admin" a partir dos seguintes comandos no terminal:
        ```go
        <MONGODB_PATH>\bin\mongo --port 27017
        ```     
        ```typescript
        use admin
        ```     
        ```go
        db.createUser({user: "admin",pwd: "admin",roles: [ { role: "readWriteAnyDatabase", db: "admin" }, { role: "userAdminAnyDatabase", db: "admin" }, { role: "dbAdminAnyDatabase", db: "admin" } ]})
        ```     
        
    4. Criar usuário "test" a partir dos seguintes comandos no terminal: 
        ```go
        //Caso já esteja conectado, descartar esse passo
        <MONGODB_PATH>\bin\mongo --port 27017
        ```     
        ```go
        use test
        ```     
        ```go
        db.createUser({user: "test",pwd: "test",roles: [ { role: "readWrite", db: "test" }, { role: "dbAdmin", db: "test" }, { role: "userAdmin", db: "test" } ]})
        ```        
    5. Fechar terminal do MongoDB do passo 2  
    
    6. Executar o mongodb Standalone no terminal com autenticação: 
        ```go
        <MONGODB_PATH>\bin\mongod --port 27017 --dbpath <YOUR_PATH>\mongodb_data\db --auth
        ``` 
    7. Executar o arquivo **set_local_variables.bat**. 
       

3. **Executar o projeto no terminal a partir da raiz deste projeto**  
    ```bash
    npm start
    ```

    *A aplicação executará em modo de desenvolvimento na porta definida pelo arquivo *set_local_variables*. Ou na porta 3000, caso não exista a variável SERVER_PORT. Abra o navegador e digite o endereço [http://localhost:3000](http://localhost:3000)*.

4. **Executar em modo debug**  
    ```bash
    npm run debug
    ```
    
## **Acceso a aplicação e banco de dados**    

* Caso precise acessar o mongodb via terminal ou cliente, utilize a seguinte string de conexão:  
    * **Local**: *mongodb://admin:admin@localhost:27017/admin*  
    * **Remoto**: *mongodb://admin:admin@**[URL_DO_ENDEREÇO]**:27017/admin*

* Caso deseje acessar os serviços rest da aplicação, utilize o arquivo **node-ts-template.json** do Insomnia para importar as requisições:
    1. Utilize o serviço **http://localhost:3000/security/logon** para gerar um token de acesso.
    2. Utilize o novo token gerado nos demais serviços através do header **x-token-app**:
    
        * POST http://localhost:3000/api/place  
        * GET http://localhost:3000/api/place  
        * GET http://localhost:3000/api/place/{id}  